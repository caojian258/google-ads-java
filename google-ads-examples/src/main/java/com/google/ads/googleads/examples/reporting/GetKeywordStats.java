// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.google.ads.googleads.examples.reporting;

import com.beust.jcommander.Parameter;

import com.google.ads.googleads.examples.utils.CodeSampleParams;
import com.google.ads.googleads.lib.GoogleAdsClient;
import com.google.ads.googleads.v6.common.Metrics;
import com.google.ads.googleads.v6.common.Segments;
import com.google.ads.googleads.v6.errors.GoogleAdsError;
import com.google.ads.googleads.v6.errors.GoogleAdsException;
import com.google.ads.googleads.v6.resources.*;

import com.google.ads.googleads.v6.services.GoogleAdsRow;
import com.google.ads.googleads.v6.services.GoogleAdsServiceClient;
import com.google.ads.googleads.v6.services.SearchGoogleAdsStreamRequest;
import com.google.ads.googleads.v6.services.SearchGoogleAdsStreamResponse;
import com.google.api.gax.rpc.ServerStream;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Gets keyword performance statistics for the 50 keywords with the most impressions over the last 7
 * days.
 */
public class GetKeywordStats {
  public GetKeywordStats() {
  }

  public static void main(String[] args) throws IOException {
    GetKeywordStats.GetGEOParams params = new GetKeywordStats.GetGEOParams();
    if (!params.parseArguments(args)) {
      params.customerId = Long.parseLong("8911666409");
    }

    GoogleAdsClient googleAdsClient;
    try {
      File propertiesFile = new File(params.properties);
      googleAdsClient = GoogleAdsClient.newBuilder().fromPropertiesFile(propertiesFile).build();
    } catch (FileNotFoundException var7) {
      System.err.printf("Failed to load GoogleAdsClient configuration from file. Exception: %s%n", var7);
      return;
    } catch (IOException var8) {
      System.err.printf("Failed to create GoogleAdsClient. Exception: %s%n", var8);
      return;
    }

    try {
      System.out.println("run");
      (new GetKeywordStats()).runExample(googleAdsClient, params.customerId, params.DT, params.outputpath);
    } catch (GoogleAdsException var9) {
      System.err.printf("Request ID %s failed due to GoogleAdsException. Underlying errors:%n", var9.getRequestId());
      int i = 0;
      Iterator var5 = var9.getGoogleAdsFailure().getErrorsList().iterator();
      while(var5.hasNext()) {
        GoogleAdsError googleAdsError = (GoogleAdsError)var5.next();
        System.err.printf("  Error %d: %s%n", i++, googleAdsError);
      }
    }
  }

  private void runExample(GoogleAdsClient googleAdsClient, long customerId, String dt, String outPutPath) {
    try {
      GoogleAdsServiceClient googleAdsServiceClient = googleAdsClient.getLatestVersion().createGoogleAdsServiceClient();
      Throwable var7 = null;

      try {
        //旧版的字段 地理位置和广告系列
        String searchQuery = "SELECT campaign.id, campaign.name, customer.currency_code, ad_group.id, ad_group.name, geographic_view.location_type ,geographic_view.country_criterion_id ,segments.date ,metrics.cost_micros ,metrics.impressions, metrics.clicks, metrics.conversions FROM geographic_view  WHERE segments.date =  '" + dt + "' ORDER BY metrics.impressions DESC ";
        //String searchQuery = "SELECT campaign.id,campaign.name,ad_group.id,ad_group.name,ad_group_ad.ad.id,ad_group_ad.ad.name,segments.date,metrics.cost_micros ,metrics.impressions, metrics.clicks   FROM ad_group_ad  WHERE segments.date =  '" + dt + "' ";
        SearchGoogleAdsStreamRequest request = SearchGoogleAdsStreamRequest.newBuilder().setCustomerId(Long.toString(customerId)).setQuery(searchQuery).build();
        ServerStream<SearchGoogleAdsStreamResponse> stream = googleAdsServiceClient.searchStreamCallable().call(request);

        List resultlist = new ArrayList();
        Iterator var12 = stream.iterator();
        while(var12.hasNext()) {
          SearchGoogleAdsStreamResponse response = (SearchGoogleAdsStreamResponse)var12.next();
          Iterator var14 = response.getResultsList().iterator();

          while(var14.hasNext()) {
            GoogleAdsRow googleAdsRow = (GoogleAdsRow)var14.next();
            StringBuilder sb = new StringBuilder();
            Campaign campaign = googleAdsRow.getCampaign();
            AdGroup adGroup=googleAdsRow.getAdGroup();
            AdGroupAd adGroupAd =googleAdsRow.getAdGroupAd();
            Metrics metrics = googleAdsRow.getMetrics();
            googleAdsRow.getAccountBudget();
            Segments segments = googleAdsRow.getSegments();
            String  ad_name= adGroupAd.getAd().getName();
            Customer customer = googleAdsRow.getCustomer();
            GeographicView geographicView = googleAdsRow.getGeographicView();
            if(ad_name==""||ad_name==null){
              ad_name="null";
            }
            /*老版本的设计是 需要进行getvalue() 取值 ，在V6版本删除。
            * */
            sb.append(segments.getDate()).append("\t").
                    append(geographicView.getCountryCriterionId()).append("\t")
                    .append(campaign.getId()).append("\t")
                    .append(campaign.getName()).append("\t")
                    .append(adGroup.getId()).append("\t")
                    .append(adGroup.getName()).append("\t")
                    .append(metrics.getCostMicros()).append("\t")
                    .append(metrics.getImpressions()).append("\t")
                    .append(metrics.getClicks()).append("\t")
                    .append(metrics.getConversions()).append("\t")
                    .append(customer.getCurrencyCode()).append("\t");
            resultlist.add(sb.toString());
          }
        }

        FileUtils.writeLines(new File(outPutPath), "utf-8", resultlist);
      } catch (Throwable var31) {
        var7 = var31;
        throw var31;
      } finally {
        if (googleAdsServiceClient != null) {
          if (var7 != null) {
            try {
              googleAdsServiceClient.close();
            } catch (Throwable var30) {
              var7.addSuppressed(var30);
            }
          } else {
            googleAdsServiceClient.close();
          }
        }
      }
    } catch (IOException var33) {
      var33.printStackTrace();
    }
  }
  private static class GetGEOParams extends CodeSampleParams {
    @Parameter(
            names = {"--customerId"},
            required = true
    )
    private Long customerId;
    @Parameter(
            names = {"--DT"},
            required = true
    )
    private String DT;
    @Parameter(
            names = {"--outputpath"},
            required = true
    )
    private String outputpath;
    @Parameter(
            names = {"--properties"},
            required = true
    )
    private String properties;

    private GetGEOParams() {
      this.DT = "2020-09-30";
      this.outputpath = "D:\\goolge_rep_geo.tsv";
      this.properties = "D:\\google-ads-java\\google-ads-examples\\src\\main\\resources\\ads.properties";
    }
  }
}